<?php

namespace App\Http\Controllers;

use App\Model\Product;
use Illuminate\Http\Request;

class ProductApiController extends Controller
{
    public function index($curentpage)
    {
        $offset=($curentpage-1)*5;
        $products = Product::limit(5)->offset($offset)->get();
        return response()->json($products);
    }
    public function latestproduct()
    {
       
        $products = Product::limit(5)
        ->orderBy('created_at','desc')->get();
        $data=[];
        $data['products']=$products;
        $data['link_img']=asset('uploads');
        return response()->json($data);
    }

    public function show($id)
    {
        $product = Product::find($id);
        if ($product) {
            return response()->json($product);
        } else {
            return response()->json(['error' => 'Product not found.'], 404);
        }
    }

    public function store(Request $request)
    {
        $data=$request->all();
        $data['image']=$this->UploadImageBase64ToFile($request->image);
        $product = Product::create($data);
        return response()->json($product, 201);
    }
    public function UploadImageBase64ToFile($base64){
        $base64Image = $base64; // Replace with your base64 image string
        // appliaction:image/png;gsqdhgsejfhzjehfejzhfjerhfjhsdrjfejfhejf
        // Extract the image data and MIME type
        // list($type, $data) = explode(';', $base64Image);
        // list(, $data) = explode(',', $data);
        
        // Decode the base64 data
        $imageData = base64_decode($base64);
        $filename = uniqid() . '.png';
        $filePath = public_path('uploads/' . $filename); 
        file_put_contents($filePath, $imageData);
        return $filename;
    }

    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        if ($product) {
            $product->update($request->all());
            return response()->json($product);
        } else {
            return response()->json(['error' => 'Product not found.'], 404);
        }
    }

    public function destroy($id)
    {
        $product = Product::find($id);
        if ($product) {
            $product->delete();
            return response()->json(['message' => 'Product deleted.']);
        } else {
            return response()->json(['error' => 'Product not found.'], 404);
        }
    }
}
