<?php

namespace App\Http\Controllers;

use App\Model\Product;
use Auth;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::latest()->paginate(5);
        return view('pages.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* simple method */
        // $product=new Product();
        // $product->name=$request->name;
        // $product->description=$request->description;
        // $product->price=$request->price;
        // $product->qte=$request->qte;
        // $product->save();
        // return redirect('product');

        // end simple methode

        // methode 2

        $request->validate([
            'name' => 'required',
        ]);
        // Retrieve the file from the request
        $file = $request->file('image');
// dd($file);
        // Generate a unique name for the file
        $fileName = time() . '_' . $file->getClientOriginalName();

        // Move the file to the desired location (in this example, the public 'uploads' directory)
        $file->move(public_path('uploads'), $fileName);
        $data=$request->all();
        $data['image']=$fileName;
        Product::create($data);

        return redirect()->route('product.index')
            ->with('success', 'Product created successfully.');

        //en dmethode 2
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {

        return view('pages.products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $product->update($request->all());
        return redirect()->route('product.index')
            ->with('success', 'Product changed successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return redirect()->route('product.index')
            ->with('success', 'Product delated.');

    }
}