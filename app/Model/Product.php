<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table="produits";
    protected $fillable = [
        'name', 'description', 'price','qte','image'
    ];
}
