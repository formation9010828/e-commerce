@extends('layout')

@section('title', 'About us')

@section('content')
    <h1>Welcome to the ab </h1>
    <p>
        @isset($msg)
        {{ $msg }}
        @endisset
       </p>
    <p>
        @isset($msg1)
        {{ $msg1 }}
        @endisset
    </p>
    <table border="1">
        <thead>
            <tr>
                <th>#</th>
                <th>Nom</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($array as $key => $name)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td> {{ $name }}</td>
                </tr>
        </tbody>
        @endforeach
        <tfoot>
            <tr>
                <td>#</td>
                <td>Nom</td>
            </tr>
        </tfoot>
    </table>
    <!-- Your home page content here -->
@endsection
@section('script')
    {{-- javascript open js file --}}
@endsection
