@extends('layout')
@section('Title', 'Listes des produits')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Listes des produits</h2>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-success" href="{{ route('product.create') }}"> Create New Product</a>
                    </div>
                </div>
            </div>

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif

            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>image</th>
                        <th>Nom</th>
                        <th>price</th>
                        <th>Qty</th>
                        <th>Action</th>
                    </tr>
                </thead>
                @foreach ($products as $key => $product)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $product->name }}</td>
                        <td><img src="{{asset('uploads/'.$product->image)}}" width="50" ></td>
                        <td>{{ $product->price }}</td>
                        <td>{{ $product->qte }}</td>

                        <td>
                            <form action="{{ route('product.destroy', $product->id) }}" method="POST">

                                <a class="btn btn-info" href="{{ route('product.show', $product->id) }}">Show</a>

                                <a class="btn btn-primary" href="{{ route('product.edit', $product->id) }}">Edit</a>

                                @csrf
                                @method('DELETE')

                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </table>
            {!! $products->links() !!}
        </div>
    </section>
</div>
@endsection
