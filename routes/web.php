<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['auth']], function () {
    Route::get('/dashboard', 'DashboardController@index');
    Route::resource('/product','ProductController');
    // Route::get('/product/create', 'ProductController@create')->name('products.create');
    Route::get('/about', 'AboutUsController@index');
    Route::Post('/about/savename', 'AboutUsController@store')->name('about.savename');
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');